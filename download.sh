#!/bin/bash

DOWNLOAD_SITE="http://xenomai.org/downloads/xenomai/stable/"

# restore timestamps using git log
for FILE in $(git ls-files)
do
    TIME=$(git log --pretty=format:%cd -n 1 --date=iso $FILE)
    TIME=$(date -d "$TIME" +%Y%m%d%H%M.%S)
    touch -m -t $TIME $FILE
done

# download files in $DOWNLOAD_SITE
wget -e robots=off -r -l1 -nd --no-parent --adjust-extension --accept=bz2,sig -w 5 --random-wait -nc $DOWNLOAD_SITE -P ./archive
