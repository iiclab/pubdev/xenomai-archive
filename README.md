# Xenomai Archive

This repository is clone of **http://xenomai.org/downloads/xenomai/stable/**.

# How to update the xenomai archives.

```bash
git clone https://git.kangwon.ac.kr/iic/xenomai-archive.git
cd xenomai-archive
chmod +x download.sh
./download.sh
git add *
git commit -m "update xenomai archives"
git push
```
